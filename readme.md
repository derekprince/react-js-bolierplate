# React Boilerplate
This is a simple, opinionated boilerplate to help quickly setup a react-redux appliaction

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

Many of the boilerplate insprirations were drawn from this [article](https://hackernoon.com/redux-step-by-step-a-simple-and-robust-workflow-for-real-life-apps-1fdf7df46092) 

### Prerequisites
* npm
* node js

### Installing
clone the repo
```
git clone https://github.com/muneneevans/react-js-bolierplate.git
```

install node modules
```
npm install
```

#### For Windows users
Before you proceed, take note of a few changes to allow for a more comfortable experience while
using this boilerplate in Windows OS.

Open the `package.json` file. Found in
```
react-js-boilerplate/
    ...
    package.json
    ...
```

Now, lets make the appropriate changes to the file.

Locate the snippet below
```json
{
"scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start": "npm run build",
    "build": "webpack -d && cp src/index.html public/index.html && webpack-dev-server --content-base src/  --inline --hot --history-api-fallback",
    "build:prod": "webpack -p && cp src/index.html dist/index.html"
  }
 }
```
Take note of the `&&` and replace all occurrences with `&` 

Next, take note of the `cp` and replace all occurrences with `copy`.

Now your snippet should look like so
```json
{
"scripts": {
    "test": "echo \"Error: no test specified\" & exit 1",
    "start": "npm run build",
    "build": "webpack -d & copy src/index.html public/index.html & webpack-dev-server --content-base src/  --inline --hot --history-api-fallback",
    "build:prod": "webpack -p & copy src/index.html dist/index.html"
  }
 }
```

## Folder Structure
After creation, your project should look like this:

```
react-js-boilerplate/
    public/
        build/
            main.js
            main.css
        index.html
    node_modules/
    src/        
        Components/
        Containers/
        Services/
        Store/
            confitureStore.js
            rootReducer.js
        app.css
        index.html
        index.js
    package.json
    readme.md
    webpack.config.js
```

## Running the tests

run test

```
npm test
```

## Buidling the project

run build
```
npm start
```

## Adding Actions/ActionTypes/Reducer/Selectors
The store is split up into domains as follow
```
Store/
    Domain1/
        actionTypes.js
        actions.js
        reducer.js
        selectors.js
    Domain2/
        actionTypes.js
        actions.js
        reducer.js
        selectors.js
    configureStore.js
    rootReducer.js
```

Example
```
Store/
    Posts/
        actionTypes.js
        actions.js
        reducer.js
        selectors.js
    Comments/
        actionTypes.js
        actions.js
        reducer.js
        selectors.js
    configureStore.js
    rootReducer.js
```

## Combining reducers
All reducers are combined in the rootReducer

```javascript
import { combineReducers } from "redux"

import domain1Reducer from './Domain1/reducer'
import domain2Reducer from './Domain2/reducer'

const rootReducer = combineReducers({
     domain1Reducer, domain2Reducer
})

export default rootReducer
```

## Configure the Store
The configureStore.js is already configured to accept the root reducer as the default store

## Built With

* [Node](https://nodejs.org/) - The web framework used
* [React](https://facebook.github.io/react/) - Componenet builder
* [Redux](http://redux.js.org/) - Js Flux management
* [Seamless Immutable](https://github.com/rtfeldman/seamless-immutable.git) - compiling scripts
* [Webpack](https://webpack.js.org/) - Dependency Management
* [Babel](https://babeljs.io/) - compiling scripts

## Authors

* **Derek Prince** - [Scedar Technologies Co.](https://scedar.bitbucket.io/)


See also the list of [contributors](https://github.com/muneneevans/react-js-bolierplate/graphs/contributors) who participated in this project.

## License

This project is licensed under the MIT License 

## Acknowledgments

* God
* [scedar](https://scedar.bitbucket.io/)
* [Evans Munene](https://github.com/muneneevans)
* [Brian Savatia](https://github.com/savatia)
* [Vorane Development Team](http://www.vorane.com)

